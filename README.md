# InSpec Framework

## Installation
The framework requires rvm or rbenv to manage framework dependencies.

* [rvm](https://rvm.io/)
* [rbenv](http://rbenv.org/)

Then you need to run:

```ruby
gem install bundler #if its not installed in your gemset
bundle install #to install the gems
```
## Run Tests (Remote)
```ruby
inspec exec [file|directory]
```

## Create Profile
A profile is a set of controls(tests) to organize the scope and code reuse.

```ruby
inspec init profile profile-name
```
to check the profiles inside the repo

```ruby
inspec check profile-name
```
output example:

```bash
Location:    demo-tests
Profile:     demo-tests
Controls:    5
Timestamp:   2018-09-21T12:50:34-03:00
Valid:       true

No errors or warnings
```
## Execute Test (Local)

```kitchen test [suite]```

## Reference

* [Inspec](https://www.inspec.io)
