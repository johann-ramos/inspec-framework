control "ssl_certificates_version" do
  impact 1.0
  title "Verify SSL Certificate"
  desc "Checks the validity of each certificate"

  describe x509_certificate('/etc/ssl/certs/apache-selfsigned.crt') do
    its('validity_in_days') { should be > 50 }
  end

  describe x509_certificate('/etc/ssl/certs/apache-selfsigned.crt') do
    its('validity_in_days') { should be > 8 }
  end

end
