control "python3.7.0" do
  impact 1.0
  title "Verify Python3.7.0 is installed"
  desc "Python3 is required to run ansible"
  describe command('which python3') do
   its('stdout') { should eq "/usr/bin/python3\n" }
  end
end
