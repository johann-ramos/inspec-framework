control "vim" do
  impact 1.0
  title "Verify Packages"
  desc "Checks the base packages"
  describe package('vim') do
    it { should be_installed }
  end
end
