control "debian_version" do
  impact 1.0
  title "Verify Debian Version"
  desc "Checks the corrent ubuntu base image"
  describe file('/etc/debian_version') do
   its('content') { should match "buster/sid" }
  end
end
